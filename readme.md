# Setup

1. Check out the git repo
1. ```docker volume create dockertest-elasticsearch-data-development```
1. ```docker volume create dockertest-database-data-development```
1. ```docker network create dockertest-development```
1. Run ```docker-compose -f docker-compose-efk.yml up``` to start elasticsearch and fluent
1. Give it a minute to start up
1. Run ```docker-compose up```
1. Confirm successful running of the setup OR expected error:

>	dockertest-database-development | WARNING: no logs are available with the 'fluentd' log driver

>	ubuntu_1         | WARNING: no logs are available with the 'fluentd' log driver

>	dockertest_ubuntu_1 exited with code 0

Docker version 20.10.2, build 2291f61
docker-compose version 1.28.2, build unknown
